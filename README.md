# Kata SG

Joseph NOUYRIGAT - Kata SG

## Add your files

## Integrate with your tools

## Collaborate with your team

## Test and Deploy
Use the Account.main(...) method.

***

## Name
Kata SG.

## Description
Kata SG (of Joseph NOUYRIGAT) including account deposit, withdrawal and history operations.

## Badges
No badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Kata SG only.

## Support
No support.

## Roadmap
No roadmap.

## Contributing
No contributions.

## Authors and acknowledgment
Joseph NOUYRIGAT.

## License
For open source projects, say how it is licensed.

## Project status
No status.
