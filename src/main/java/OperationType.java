package src.main.java;

public enum OperationType {
    DEPOSIT,
    WITHDRAWAL,
    HISTORY
}
