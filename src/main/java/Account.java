package src.main.java;

import java.util.ArrayList;
import java.util.Scanner;

public class Account {

    private static final double MAX_AMOUNT = 1000000000; // too big numbers can produce errors
    private double amount;
    private final ArrayList<Operation> operations;

    public Account() {
        amount = 0;
        operations = new ArrayList<>();
    }

    public static double getMaxAmount() {
        return MAX_AMOUNT;
    }

    public double deposit(double aDeposit) {
        if (aDeposit < 0 || aDeposit > MAX_AMOUNT || amount + aDeposit > MAX_AMOUNT)
            return 0;
        amount += aDeposit;
        operations.add(new Operation(OperationType.DEPOSIT, aDeposit, amount));
        return amount;
    }

    public double withdrawal(double aWithdrawal) {
        if (aWithdrawal < 0 || aWithdrawal > amount)
            return 0;
        amount -= aWithdrawal;
        operations.add(new Operation(OperationType.WITHDRAWAL, aWithdrawal, amount));
        return amount;
    }

    public void history() {
        operations.add(new Operation(OperationType.HISTORY, 0, amount));
    }

    public static void main(String[] args) {
        Account account = new Account();
        Scanner sc = new Scanner(System.in);
        System.out.println("Possible operation and formats:");
        System.out.println("Deposit : DEPOSIT XXX.XX");
        System.out.println("Exit : EXIT");

        boolean exit = false;
        while (!exit) {
            System.out.println("Please insert an operation.");
            String[] line = sc.nextLine().split(" ");
            if (OperationType.DEPOSIT.name().equals(line[0])) {
                try {
                    double d = Double.parseDouble(line[1]);
                    double result = account.deposit(d);
                    if (result == -1)
                        System.out.println("Too big number (maximum is:" + MAX_AMOUNT + ")");
                    else
                        System.out.println("New amount after deposit: " + result + ".");
                } catch (NumberFormatException e) {
                    System.out.println("Wrong number");
                }
            } else if (OperationType.WITHDRAWAL.name().equals(line[0])) {
                try {
                    double d = Double.parseDouble(line[1]);
                    System.out.println("New amount after withdrawal: " + account.withdrawal(d) + ".");
                } catch (NumberFormatException e) {
                    System.out.println("Wrong number");
                }
            } else if (OperationType.HISTORY.name().equals(line[0])) {
                try {
                    for (Operation o: account.operations)
                        if (o.getOperationType().equals(OperationType.HISTORY))
                            System.out.println(o.getOperationType().name() + " the " + o.getDate().toString());
                        else System.out.println(o.getOperationType().name() + " for " + o.getAmount() +
                                " euros. Balance after operation is " + o.getBalance() + " the " + o.getDate().toString());

                    account.history();
                } catch (NumberFormatException e) {
                    System.out.println("Wrong number");
                }
            } else if ("EXIT".equals(line[0])) {
                System.out.println("Good bye!");
                exit = true;
            } else {
                System.out.println("Unknown operation, please retry.");
            }
        }
    }
}
