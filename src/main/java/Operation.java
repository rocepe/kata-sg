package src.main.java;

import java.time.LocalDate;

public class Operation {
    private final OperationType operationType;
    private final LocalDate date;
    private final double amount;
    private final double balance; // account balance after operation

    protected Operation(OperationType aOperationType, double aAmount, double aBalance) {
        operationType = aOperationType;
        date = LocalDate.now();
        amount = aAmount;
        balance = aBalance;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public LocalDate getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }

    public double getBalance() {
        return balance;
    }
}
