package src.test.java;

import org.junit.Assert;
import org.junit.Test;
import src.main.java.Account;

public class TestAccountWithdrawal {

    @Test
    public void testWithdrawalEmptyAccount() {
        Account account = new Account();
        Assert.assertEquals(0, account.withdrawal(100), 0);
    }

    @Test
    public void testWithdrawalAccountPossible() {
        Account account = new Account();
        account.deposit(150);
        Assert.assertEquals(0,
                account.withdrawal(-50), 0);
        Assert.assertEquals(100,
                account.withdrawal(50), 0);
        Assert.assertEquals(70,
                account.withdrawal(30), 0);
        Assert.assertEquals(0,
                account.withdrawal(70), 0);
    }

    @Test
    public void testWithdrawalAccountTooBigValue() {
        Account account = new Account();
        account.deposit(150);
        Assert.assertEquals(0,
                account.withdrawal(1000), 0);
    }
}
