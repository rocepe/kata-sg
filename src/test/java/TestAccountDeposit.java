package src.test.java;

import org.junit.Assert;
import org.junit.Test;
import src.main.java.Account;

public class TestAccountDeposit {

    @Test
    public void testDepositEmptyAccount() {
        Account account = new Account();
        Assert.assertEquals(100, account.deposit(100), 0);
        Assert.assertEquals(100, account.deposit(-100), 0);
        Assert.assertEquals(350, account.deposit(250), 0);
    }

    @Test
    public void testDepositEmptyAccountTooBigValue() {
        Account account = new Account();
        Assert.assertEquals(0,
                account.deposit(Account.getMaxAmount() + 1), 0);
    }
}
